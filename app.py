import sys
import browsersetting
import logintest
import posttext
import postimage

def main():
    print("The script has the name %s" % (sys.argv[1]))
    print("The script has the len %s" % (len(sys.argv)))

    if len(sys.argv) == 3:
        if (sys.argv[2] == "login"):
            # login Test
            listuser =[
            {"id": "MariTestDev1", "password": "sem@ngat45"},
            {"id": "MariTestDev2", "password": "sem@ngat45"},
            {"id": "MariTestDev3", "password": "sem@ngat45"},
            {"id": "MariTestDev4", "password": "sem@ngat45"},
            {"id": "MariTestDev5", "password": "sem@nga t45"}
            ]
            passed = 0
            failed = 0

            for user in listuser:
                driver = browsersetting.setting(sys.argv[1], "https://twitter.com/login")
                result = logintest.login(driver, user["id"], user["password"])
                print("test number : " + result)
                if (result == "success"):
                    passed += 1
                else:
                    failed += 1

            print("Total Login Test: " + str(len(listuser)) + " | Pass: " + str(passed) + " | Fail: " + str(failed))
            print("=======================================================================================")

        elif (sys.argv[2] == "text"):
            # ====================================================================
            list_text = [
                {"text": "Hallo Ini Tester Text #1"},
                {"text": "Hallo Ini Tester Text #2"},
                {"text": "Hallo Ini Tester Text #3"},
                {"text": "Hallo Ini Tester Text #4"},
            ]
            passed = 0
            failed = 0
            user = "MariTestDev1"
            passw = "sem@ngat45"
            # Post Test Text
            # execute post text on twitter
            for text in list_text:
                driver = browsersetting.setting(sys.argv[1], "https://twitter.com/login")
                result = logintest.loginpost(driver, user, passw)
                res = posttext.send(driver, text["text"])
                print(res)
                if (res == "success"):
                    passed += 1
                else:
                    failed += 1

            print("Total Post Text Test: " + str(len(list_text)) + " | Post: " + str(passed) + " | Fail: " + str(failed))
            print("===========================================================================================")

        elif (sys.argv[2] == "images"):
            # ====================================================================
            list_images = [
                {"img": "C:\\Users\\izzul\Pictures\\2020_05_27\\IMG_2408_als.jpg"},
                {"img": "C:\\Users\izzul\Pictures\\2020_05_27\\IMG_2409_als.jpg"},
            ]
            passed = 0
            failed = 0
            user = "MariTestDev1"
            passw = "sem@ngat45"
            # Post Images
            # execute post image on twitter
            for image in list_images:
                driver = browsersetting.setting(sys.argv[1], "https://twitter.com/login")
                result = logintest.loginpost(driver, user, passw)
                res = postimage.send(driver, image["img"])

                print(res)
                if (res == "success"):
                    passed += 1
                else:
                    failed += 1

            print("Total Post Images Test: " + str(len(list_images)) + " | Post: " + str(passed) + " | Fail: " + str(failed))
            print("===========================================================================================")

        else:
            print("invalid syntax please use \"python app.py <location of webdriver> <type of test>\"")
            print("ex: python app.py F:\MASTER\ChromeDriver\chromedriver.exe login")
    else:
        print("invalid syntax please use \"python app.py <location of webdriver> <type of test>\"")
        print("example: python app.py F:\MASTER\ChromeDriver\chromedriver.exe login")


if __name__ == "__main__":
    main()








