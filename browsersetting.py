from selenium import  webdriver

def setting(loc_chromedriver, url):
    # create a new session
    drive =  webdriver.Chrome(loc_chromedriver)
    drive.implicitly_wait(5)
    drive.maximize_window()
    # Navigate to the application (login Twitter)
    drive.get(url)

    return drive

