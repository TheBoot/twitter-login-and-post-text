from selenium.webdriver.support.ui import WebDriverWait

def send(driver, image):
    try:
        # Post Image
        # CLick Image icon
        driver.find_element_by_css_selector(".r-13gxpu9").click()
        # Select Element
        input = driver.find_element_by_css_selector(".r-8akbif")
        # # Input images
        input.send_keys(image)
        # Click Button Tweet
        driver.find_element_by_xpath("//div[2]/div[3]/div").click()
        WebDriverWait(driver,20)

        result = "success"
        return result

    except:
        result = "failed"
        driver.quit()
        return result