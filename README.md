# Twitter Login, Post text, and Images

dependencies
- python3.7
- selenium

# How to install dependencies
**pip install -r requirements.txt**

# Type of test
- login
- text
- images

# How to run
**python app.py (location of webdriver) (type of test)**

# Example
- login test => **python app.py F:\MASTER\ChromeDriver\chromedriver.exe login**
- login post text => **python app.py F:\MASTER\ChromeDriver\chromedriver.exe text**
- login post image => **python app.py F:\MASTER\ChromeDriver\chromedriver.exe images**