
def login(driver, username, password):
    try:
        # Login Test
        driver.find_element_by_name("session[username_or_email]").send_keys(username);
        driver.find_element_by_name("session[password]").send_keys(password);
        driver.find_element_by_xpath("//div[3]/div/div").click()
        # Validate User Success Login Or Not
        driver.find_element_by_css_selector(".r-1twgtwe").click()
        validuser = f"@{username}"
        uservalidate = f"//span[contains(.,'{validuser}')]"
        user =  driver.find_element_by_xpath(uservalidate).text

        if (user == validuser):
            result = "success"
            driver.quit()
            return result

    except:
        result = "failed"
        driver.quit()
        return result

def loginpost(driver, username, password):
    try:
        # Login Test
        driver.find_element_by_name("session[username_or_email]").send_keys(username);
        driver.find_element_by_name("session[password]").send_keys(password);
        driver.find_element_by_xpath("//div[3]/div/div").click()
        # Validate User Success Login Or Not
        # driver.find_element_by_css_selector(".r-1twgtwe").click()
        # validuser = f"@{username}"
        # uservalidate = f"//span[contains(.,'{validuser}')]"
        # user =  driver.find_element_by_xpath(uservalidate).text
        #
        # if (user == validuser):
        result = "success login"
        return result

    except:
        result = "failed login"
        driver.quit()
        return result
